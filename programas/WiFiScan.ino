/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Despliega redes WiFi disponibles
 */
#include "WiFi.h"

void setup()
{
    Serial.begin(115200);

    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);

    Serial.println("Inicialización terminada");
}

void loop()
{
    Serial.println("Buscando redes");

    // WiFi.scanNetworks retorna el número de redes detectadas.
    int n = WiFi.scanNetworks();
    Serial.println("Busqueda terminada");
    if (n == 0) {
        Serial.println("No se localizaron redes WiFi disponibles");
    } else {
        Serial.print(n);
        Serial.println(" redes WiFi disponibles");
        for (int i = 0; i < n; ++i) {
            // Imprime el SSID de las redes detectadas
            Serial.print(i + 1);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.println();
            delay(10);
        }
    }
    Serial.println("");

    // Espera para volver a buscar redes disponibles.
    delay(5000);
}
