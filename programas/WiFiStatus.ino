/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"

const char * ssid="hercab2k20";
const char * passwd="AxVa9295";


void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED)
        Serial.println("Conexión completada correctamente!!");
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}

void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
}

void loop()
{
    Serial.print("Estado de la conexión: ");
    switch(WiFi.status()){
      case WL_CONNECTED: Serial.println(" Conectada"); break;
      case WL_NO_SHIELD: Serial.println(" WiFi no existe!!"); break;
      case WL_NO_SSID_AVAIL: Serial.println(" SSID no disponible!!"); break;
      case WL_CONNECT_FAILED: Serial.println(" Intento de conexión fallida!!"); break;
      case WL_CONNECTION_LOST : Serial.println(" Conexión perdida!!"); break;
      case WL_DISCONNECTED : Serial.println(" Desconetada!!"); break;
      default: Serial.println(" Estado desconocido!!");
    }
    
    delay(3000);
}
